## Using Git for DBLab Engine configuration management

This is an example of a repository that demonstrates a how to manage the configuration of the DBLab Engine using Git.

#### Requirements

- You will need the `Org key` and `Project name` from the [Postgres.ai platform](https://console.postgres.ai). These are provided by the platform upon registration. You can find more details [here](https://postgres.ai/docs/how-to-guides/administration/install-dle-from-postgres-ai).
  - Keep in mind that without specifying these values in the `platform_org_key` and `platform_project_name` variables, the Ansible Playbook will not be executed.

#### Steps

1. [Deploy](https://postgres.ai/docs/how-to-guides/administration/install-dle-from-postgres-ai) DBLab Engine
2. Get the DBLab Engine configuration file. Can be copied from the server, along the path `.dblab/engine/configs/server.yml`
3. Replace the values of the secret parameters in the configuration file with variables in the format: `{{ variable_name }}`, and upload it to your repository.
4. Add variables to `CI/CD Settings` for all secret values, example:

![TypeB](variables.png)

5. Add your variables to `--extra-vars`, see the sample `.gitlab-ci.yml` file in this repository.
6. Use runner whose public key is added to the DBLab Engine server, or add the private key to the `SSH_PRIVATE_KEY` variable as in the example of this repository.

#### About IaC and GitOps:

`Infrastructure as Code (IaC)` is the managing and provisioning of infrastructure through code instead of through manual processes.

`GitOps` automates infrastructure updates using a Git workflow with continuous integration (CI) and continuous delivery (CD).\
When new code is merged, the CI/CD pipeline enacts the change in the environment. Any configuration drift, such as manual changes or errors, is overwritten by GitOps automation so the environment converges on the desired state defined in Git.

Details:

- [What is GitOps](https://about.gitlab.com/topics/gitops/)
- [What is Infrastructure as Code (IaC)](https://www.redhat.com/en/topics/automation/what-is-infrastructure-as-code-iac)


#### Additional Resources:

- [DBLab Engine repository](https://gitlab.com/postgres-ai/database-lab)
- [How to install DBLab Engine from Postgres.ai Console](https://postgres.ai/docs/how-to-guides/administration/install-dle-from-postgres-ai)
- [Automation of the DBLab Engine using Ansible](https://gitlab.com/postgres-ai/dle-se-ansible)
